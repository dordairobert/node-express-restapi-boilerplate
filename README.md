###### For learning purposes only, not production ready

---

* MongoDB ORM: [mongoose](https://www.npmjs.com/package/mongoose)
* Request validator: [joi](https://www.npmjs.com/package/joi)
* Linting: [eslint](https://www.npmjs.com/package/eslint)
* Testing: [mocha](https://www.npmjs.com/package/mocha), [chai](https://www.npmjs.com/package/chai) & [sinon](https://www.npmjs.com/package/sinon) 
* Authentication: [passport](https://www.npmjs.com/package/passport)
* Monitoring: [pm2](https://www.npmjs.com/package/pm2)