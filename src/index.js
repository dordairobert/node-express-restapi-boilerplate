Promise = require("bluebird");
const app = require("./config/express");
const { port, env } = require("./config/vars");
const mongoose = require("./config/mongoose");
const chalk = require("chalk");

mongoose.connect();

app.listen(port, () => {
  console.info(
    chalk.black.bgWhite.bold(
      `server started on port ${port} ` + chalk.red.bgWhite.bold(`(${env})`)
    )
  );
});

module.exports = app;
