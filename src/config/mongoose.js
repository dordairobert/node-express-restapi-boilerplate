const mongoose = require("mongoose");
const { mongo, env } = require("./vars");
const chalk = require("chalk");

mongoose.Promise = Promise;

mongoose.connection.on("open", err => {
  console.info(chalk.black.bgWhite.bold("Connected to MongoDB"));
});

mongoose.connection.on("error", err => {
  console.error(chalk.white.bgRed.bold(`MongoDB connection error: ${err}`));
  process.exit(-1);
});

if (env === "development") {
  mongoose.set("debug", true);
}

exports.connect = () => {
  mongoose.connect(
    mongo.uri,
    mongo.options
  );
  return mongoose.connection;
};
