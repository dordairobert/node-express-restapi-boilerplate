const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const compress = require("compression");
const methodOverride = require("method-override");
const helmet = require("helmet");
const passport = require("passport");
const strategies = require("./passport");
const error = require("../api/middlewares/error");

const routes = require("../api/routes/v1");
const { logs } = require("./vars");

const app = express();

app.use(morgan(logs));

app.use(
  bodyParser.json({
    limit: "300kb"
  })
);

app.use(bodyParser.urlencoded({ extended: true }));

app.use(compress());

app.use(methodOverride());

app.use(
  helmet({
    hidePoweredBy: {
      setTo: "PHP 7.0.1"
    }
  })
);

app.use(passport.initialize());
passport.use("jwt", strategies.jwt);

app.use("/v1", routes);

app.use(error.converter);
app.use(error.notFound);
app.use(error.handler);

module.exports = app;
