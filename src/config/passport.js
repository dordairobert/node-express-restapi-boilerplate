const JwtStrategy = require("passport-jwt").Strategy;
const { ExtractJwt } = require("passport-jwt");
const { jwtSecret } = require("./vars");
const User = require("../api/services/user/user-model");
const on = require("await-handler");

const jwtOptions = {
  secretOrKey: jwtSecret,
  jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("Bearer")
};

const jwt = async (payload, done) => {
  const [error, user] = await on(User.findById(payload.sub));
  if (error) {
    return done(error, false);
  }
  if (user) {
    return done(null, user);
  }
  return done(null, false);
};

exports.jwt = new JwtStrategy(jwtOptions, jwt);
