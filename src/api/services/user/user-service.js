const { omit } = require("lodash");
const User = require("./user-model");
const on = require("await-handler");

exports.get = async id => User.get(id);

exports.loggedIn = (req, res) => res.json(req.user.transform());

exports.create = async userData => {
  const user = new User(userData);
  const [error, savedUser] = await on(user.save());
  if (error) {
    throw User.checkDuplicateEmail(error);
  }
  return savedUser.transform();
};

exports.replace = async (user, newUserData) => {
  const newUser = new User(newUserData);
  const ommitRole = user.role !== "admin" ? "role" : "";
  const newUserObject = omit(newUser.toObject(), "_id", ommitRole);

  let error;
  [error] = await on(
    user.updateOne(newUserObject, { override: true, upsert: true })
  );
  if (error) {
    throw User.checkDuplicateEmail(error);
  }
  let savedUser;
  [error, savedUser] = await on(User.findById(user._id));
  if (error) {
    throw error;
  }

  return savedUser.transform();
};

exports.update = async (user, updatedData) => {
  const ommitRole = user.role !== "admin" ? "role" : "";
  const userToBeUpdated = omit(updatedData, ommitRole);
  const updatedUser = Object.assign(user, userToBeUpdated);
  const [error, savedUser] = await on(updatedUser.save());
  if (error) {
    throw User.checkDuplicateEmail(error);
  }
  return savedUser.transform();
};

exports.list = async params => {
  const [error, users] = await on(User.list(params));
  if (error) {
    throw error;
  }
  const transformedUsers = users.map(user => user.transform());
  return transformedUsers;
};

exports.remove = async user => User.deleteOne(user);
