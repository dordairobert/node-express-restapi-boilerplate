const httpStatus = require("http-status");
const service = require("./user-service");
const { handler: errorHandler } = require("../../middlewares/error");
const on = require("await-handler");

exports.load = async (req, res, next, id) => {
  const [error, user] = await on(service.get(id));
  if (error) {
    return errorHandler(error, req, res);
  }
  req.locals = { user };
  return next();
};

exports.get = (req, res) => res.json(req.locals.user.transform());

exports.loggedIn = (req, res) => res.json(req.user.transform());

exports.create = async (req, res, next) => {
  const [error, response] = await on(service.create(req.body));
  if (error) {
    return next(error);
  }
  return res.status(httpStatus.CREATED).json(response);
};

exports.replace = async (req, res, next) => {
  const { user } = req.locals;
  const [error, response] = await on(service.replace(user, req.body));
  if (error) {
    return next(error);
  }
  return res.json(response);
};

exports.update = async (req, res, next) => {
  const { user } = req.locals;
  const [error, response] = await on(service.update(user, req.body));
  if (error) {
    return next(error);
  }
  return res.json(response);
};

exports.list = async (req, res, next) => {
  const [error, response] = await on(service.list(req.query));
  if (error) {
    next(error);
  }
  res.json(response);
};

exports.remove = async (req, res, next) => {
  const { user } = req.locals;
  const [error] = await on(service.remove(user));
  if (error) {
    next(error);
  }
  res.status(httpStatus.NO_CONTENT).end();
};
