const mongoose = require("mongoose");
const httpStatus = require("http-status");
const { omitBy, isNil } = require("lodash");
const bcrypt = require("bcryptjs");
const on = require("await-handler");
const moment = require("moment-timezone");
const jwt = require("jwt-simple");
const APIError = require("../../utils/APIError");
const {
  env,
  jwtSecret,
  jwtExpirationInterval
} = require("../../../config/vars");

const roles = ["user", "admin"];
//asd
const userSchema = new mongoose.Schema({
  email: {
    type: String,
    match: /^\S+@\S+\.\S+$/,
    required: true,
    unique: true,
    trim: true,
    lowercase: true
  },
  password: {
    type: String,
    required: true,
    minlength: 8,
    maxlength: 128
  },
  name: {
    type: String,
    maxlength: 128,
    index: true,
    trim: true
  },
  role: {
    type: String,
    enum: roles,
    default: "user"
  },
  picture: {
    type: String,
    trim: true
  },
  createdAt: {
    type: String,
    default: moment().format("MMMM Do YYYY, h:mm:ss a")
  }
});

userSchema.pre("save", async function save(next) {
  if (!this.isModified("password")) return next();
  const rounds = env === "test" ? 1 : 10;

  const [error, hash] = await on(bcrypt.hash(this.password, rounds));
  if (error) {
    return next(error);
  }
  this.password = hash;
  return next();
});

userSchema.method({
  transform() {
    const transformed = {};
    const fields = ["id", "name", "email", "picture", "role", "createdAt"];

    fields.forEach(field => {
      transformed[field] = this[field];
    });

    return transformed;
  },
  token() {
    const playload = {
      exp: moment()
        .add(jwtExpirationInterval, "minutes")
        .unix(),
      iat: moment().unix(),
      sub: this._id
    };
    return jwt.encode(playload, jwtSecret);
  },
  async passwordMatches(password) {
    return bcrypt.compare(password, this.password);
  }
});

userSchema.statics = {
  roles,
  async get(id) {
    let user, error;
    if (mongoose.Types.ObjectId.isValid(id)) {
      [error, user] = await on(this.findById(id).exec());
      if (error) {
        throw error;
      }
    }
    if (user) {
      return user;
    }

    throw new APIError({
      message: "User does not exist",
      status: httpStatus.NOT_FOUND
    });
  },
  async findAndGenerateToken(options) {
    const { email, password, refreshObject } = options;
    if (!email)
      throw new APIError({
        message: "An email is required to generate a token"
      });

    const user = await this.findOne({ email }).exec();
    const err = {
      status: httpStatus.UNAUTHORIZED,
      isPublic: true
    };
    if (password) {
      if (user && (await user.passwordMatches(password))) {
        return { user, accessToken: user.token() };
      }
      err.message = "Incorrect email or password";
    } else if (refreshObject && refreshObject.userEmail === email) {
      return { user, accessToken: user.token() };
    } else {
      err.message = "Incorrect email or refreshToken";
    }
    throw new APIError(err);
  },
  list({ page = 1, perPage = 30, name, email, role }) {
    const options = omitBy({ name, email, role }, isNil);

    return this.find(options)
      .sort({ createdAt: -1 })
      .skip(perPage * (page - 1))
      .limit(perPage)
      .exec();
  },
  checkDuplicateEmail(error) {
    if (
      error.code === 11000 &&
      (error.name === "BulkWriteError" || error.name === "MongoError")
    ) {
      return new APIError({
        message: "Validation Error",
        errors: [
          {
            field: "email",
            location: "body",
            messages: ['"email" already exists']
          }
        ],
        status: httpStatus.CONFLICT,
        isPublic: true,
        stack: error.stack
      });
    }
    return error;
  }
};

module.exports = mongoose.model("User", userSchema);
