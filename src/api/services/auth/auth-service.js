const User = require("../user/user-model");
const RefreshToken = require("./refreshToken-model");
const moment = require("moment-timezone");
const { jwtExpirationInterval } = require("../../../config/vars");
const on = require("await-handler");

function generateTokenResponse(user, accessToken) {
  const tokenType = "Bearer";
  const refreshToken = RefreshToken.generate(user).token;
  const expiresIn = moment().add(jwtExpirationInterval, "minutes");
  return {
    tokenType,
    accessToken,
    refreshToken,
    expiresIn
  };
}

exports.register = async userData => {
  const [error, user] = await on(new User(userData).save());
  if (error) {
    throw User.checkDuplicateEmail(error);
  }
  const userTransformed = user.transform();
  const token = generateTokenResponse(user, user.token());
  return { token, user: userTransformed };
};

exports.login = async userData => {
  try {
    const { user, accessToken } = await User.findAndGenerateToken(userData);
    const token = generateTokenResponse(user, accessToken);
    const userTransformed = user.transform();
    return { token, user: userTransformed };
  } catch (error) {
    throw error;
  }
};

exports.refresh = async ({ email, refreshToken }) => {
  try {
    const refreshObject = await RefreshToken.findOneAndRemove({
      userEmail: email,
      token: refreshToken
    });
    const { user, accessToken } = await User.findAndGenerateToken({
      email,
      refreshObject
    });
    return generateTokenResponse(user, accessToken);
  } catch (error) {
    throw error;
  }
};
