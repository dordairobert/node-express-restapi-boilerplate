const httpStatus = require("http-status");
const service = require("./auth-service");
const on = require("await-handler");

exports.register = async (req, res, next) => {
  const [error, response] = await on(service.register(req.body));
  if (error) return next(error);
  return res.status(httpStatus.CREATED).json(response);
};

exports.login = async (req, res, next) => {
  const [error, response] = await on(service.login(req.body));
  if (error) return next(error);
  return res.json(response);
};

exports.refresh = async (req, res, next) => {
  const [error, response] = await on(service.refresh(req.body));
  if (error) return next(error);
  return res.json(response);
};
