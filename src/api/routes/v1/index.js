const express = require("express");

const userRoutes = require("../../services/user/user-route");
const authRoutes = require("../../services/auth/auth-route");

const router = express.Router();

router.get("/status", (req, res) => res.send("OK"));

router.use("/users", userRoutes);
router.use("/auth", authRoutes);

module.exports = router;
